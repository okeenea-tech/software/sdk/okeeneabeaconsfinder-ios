Pod::Spec.new do |spec|
  spec.name = "OkeeneaBeaconsFinder"
  spec.version = "3.1.0"
  spec.summary = "Interact with beacons from Okeenea Tech."
  spec.description = <<-DESC
    OkeeneaBeaconsFinder allows you to interact with beacons from Okeenea Tech. You need an API key to use it.
  DESC
  spec.homepage = "https://www.okeenea.com/"
  spec.license = { :type => "Proprietary Software - Copyright (c) 2021-present Okeenea", :text => <<-LICENSE
    Copyright 2021 - present Okeenea

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
                   LICENSE
  }

  spec.author = "Okeenea"
  spec.platform = :ios, "12.4"
  spec.source = { :git => "https://gitlab.com/okeenea-tech/software/sdk/okeeneabeaconsfinder-ios.git", :tag => "v#{spec.version}" }
  spec.vendored_frameworks = ['OkeeneaBeaconsFinder/Frameworks/OkeeneaBeaconsFinder.xcframework']
  spec.frameworks = "UIKit", "CoreLocation", "CoreBluetooth"
  spec.dependency 'R.swift', '~> 7'
  spec.dependency 'SwifterSwift', '~> 5'
  spec.pod_target_xcconfig = { 'ONLY_ACTIVE_ARCH' => 'YES' }
  spec.swift_versions = ['4.0', '4.2', '5.0']
end
