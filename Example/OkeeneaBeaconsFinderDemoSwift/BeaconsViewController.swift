//
// Created by Nicolas VERINAUD on 21/12/2021.
//

import CoreLocation
import CoreBluetooth
import OkeeneaBeaconsFinder
import UIKit

class BeaconsViewController: UITableViewController {
    
    override init(style: UITableView.Style) {
        super.init(style: style)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        title = "Beacons Finder"
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Start", style: .plain, target: self, action: #selector(toggleStart))
    }
    
    private let locationManager = CLLocationManager()
    private let centralManager = CBCentralManager()
    private let beaconsFinder = BeaconsFinder(apiKey: ApiKey.fromAppBundle())
    private let cellId = "Cell"
    private var isStarted = false
    private var beacons: [OkeeneaBeacon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(BeaconTableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: animated)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        beacons.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        (tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! BeaconTableViewCell).updated(with: beaconAt(indexPath))
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsViewController = BeaconDetailsViewController(beacon: beaconAt(indexPath))
        navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    @objc
    private func toggleStart() {
        if isStarted {
            stop()
        } else {
            start()
        }
    }
    
    private func stop() {
        isStarted = false
        updateStartStopButtonTitle("Start")
        beaconsFinder.stopScan()
        beacons = []
        tableView.reloadData()
    }
    
    private func start() {
        if authorized() {
            startScan()
        } else {
            askForNecessaryAuthorizations()
        }
    }
    
    private func authorized() -> Bool {
        locationAuthorized() && bluetoothAuthorized()
    }
    
    private func locationAuthorized() -> Bool {
        CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways
    }
    
    private func bluetoothAuthorized() -> Bool {
        centralManager.state == .poweredOn
    }
    
    private func askForNecessaryAuthorizations() {
        if !locationAuthorized() {
            locationManager.requestWhenInUseAuthorization()
        } else if !bluetoothAuthorized() {
            presentAlert(message: "Bluetooth Unauthorized")
        }
    }
    
    private func startScan() {
        isStarted = true
        updateStartStopButtonTitle("Stop")
        beaconsFinder.startScan(onUpdate: { [weak self] beacons in
            self?.didFindBeacons(beacons)
        }, onError: { [weak self] error in
            self?.scanDidFail(error)
        })
    }
    
    private func didFindBeacons(_ beacons: [OkeeneaBeacon]) {
        self.beacons = beacons
        tableView.reloadData()
    }
    
    private func scanDidFail(_ error: OkeeneaError) {
        showError(error)
        stop()
    }
    
    private func showError(_ error: OkeeneaError) {
        presentAlert(message: error.debugDescription)
    }
    
    private func presentAlert(message: String) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
        present(alert, animated: true)
    }
    
    private func updateStartStopButtonTitle(_ title: String) {
        navigationItem.rightBarButtonItem?.title = title
    }
    
    private func beaconAt(_ indexPath: IndexPath) -> OkeeneaBeacon {
        beacons[indexPath.row]
    }
}
