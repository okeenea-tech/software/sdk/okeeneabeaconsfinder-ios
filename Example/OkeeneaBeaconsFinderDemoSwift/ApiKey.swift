//
// Created by Nicolas VERINAUD on 20/01/2022.
//

import Foundation

enum ApiKey {
    
    static func fromAppBundle() -> String {
        let url = Bundle.main.url(forResource: "API_KEY", withExtension: nil)!
        return try! String(contentsOf: url)
    }
}
