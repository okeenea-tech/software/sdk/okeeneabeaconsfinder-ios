//
// Created by Nicolas VERINAUD on 13/01/2022.
//

import UIKit
import OkeeneaBeaconsFinder

class BeaconDetailsViewController: UITableViewController {
    
    init(beacon: OkeeneaBeacon) {
        self.beacon = beacon
        super.init(style: .grouped)
        
        title = beacon.name
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    private let beacon: OkeeneaBeacon
    
    private let cellId = "CellId"
    
    private var actions: [Action] {
        beacon.canBeTriggered ? allPossibleActions() : actionsWhenBeaconCannotBeTriggered()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        actions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.textLabel?.text = actionAt(indexPath).name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        actionAt(indexPath).onPerform()
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func actionAt(_ indexPath: IndexPath) -> Action {
        actions[indexPath.row]
    }
    
    private func allPossibleActions() -> [Action] {
        let beacon = beacon
        return [
            Action(name: "Trigger 'Jingle'") {
                beacon.trigger(message: .jingle, volume: .medium, language: .english, completionHandler: self.weakDidComplete("Trigger Jingle"))
            },
            Action(name: "Trigger 'Message 1'") {
                beacon.trigger(message: .message1, volume: .medium, language: .english, completionHandler: self.weakDidComplete("Trigger Message 1"))
            },
            Action(name: "Trigger 'Message 2'") {
                beacon.trigger(message: .message2, volume: .medium, language: .english, completionHandler: self.weakDidComplete("Trigger Message 2"))
            },
            Action(name: "Trigger 'Message 3'") {
                beacon.trigger(message: .message3, volume: .medium, language: .english, completionHandler: self.weakDidComplete("Trigger Message 3"))
            },
            Action(name: "Repeat last message") {
                beacon.repeatLastMessage(completionHandler: self.weakDidComplete("Repeat last message"))
            },
            Action(name: "Play next message") {
                beacon.playNextMessage(completionHandler: self.weakDidComplete("Play next message"))
            },
            Action(name: "Cancel") {
                beacon.cancelPlayedMessage(completionHandler: self.weakDidComplete("Cancel played message"))
            }
        ]
    }
    
    private func actionsWhenBeaconCannotBeTriggered() -> [Action] {
        [Action(name: "This beacon cannot be triggered", onPerform: {})]
    }
    
    private func weakDidComplete(_ actionDescription: String) -> (Error?) -> Void {
        { [weak self] error in
            self?.didComplete(actionDescription, error)
        }
    }
    
    private func didComplete(_ actionDescription: String, _ error: Error?) {
        if let error = error {
            let vc = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
            vc.addAction(UIAlertAction(title: "Ok", style: .cancel))
            present(vc, animated: true)
        } else {
            NSLog("Did complete '\(actionDescription)'.")
        }
    }
    
    struct Action {
        var name: String
        var onPerform: () -> Void
    }
}
