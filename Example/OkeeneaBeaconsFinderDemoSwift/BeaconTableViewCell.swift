//
// Created by Nicolas VERINAUD on 21/12/2021.
//

import OkeeneaBeaconsFinder
import UIKit

class BeaconTableViewCell: UITableViewCell {
    
    override init(style: CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updated(with beacon: OkeeneaBeacon) -> Self {
        textLabel?.text = beacon.name
        detailTextLabel?.text = "\(String(describing: beacon.kind)) (rssi=\(beacon.proximity.rssi))"
        return self
    }
}
