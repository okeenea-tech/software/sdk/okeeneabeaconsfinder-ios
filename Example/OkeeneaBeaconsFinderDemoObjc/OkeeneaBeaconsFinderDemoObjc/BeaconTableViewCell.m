//
//  Copyright © 2021 Okeenea. All rights reserved.
//

#import "BeaconTableViewCell.h"

@implementation BeaconTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    return self;
}

- (instancetype)updatedWith:(id <OkeeneaBeacon>)beacon {
    self.textLabel.text = beacon.name;
    self.detailTextLabel.text = [NSString stringWithFormat:@"%d (rssi=%li)", (int) beacon.kind, beacon.proximity.rssi];
    return self;
}

@end
