//
//  Copyright © 2021 Okeenea. All rights reserved.
//

#import "AppDelegate.h"
#import "BeaconsViewController.h"

@interface AppDelegate ()
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];

    BeaconsViewController *viewController = [[BeaconsViewController alloc] initWithStyle:UITableViewStyleGrouped];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    navigationController.navigationBar.prefersLargeTitles = YES;

    _window.rootViewController = navigationController;
    [_window makeKeyAndVisible];

    return YES;
}

@end
