//
//  Copyright © 2021 Okeenea. All rights reserved.
//

#import "Action.h"

@interface Action ()

@property (nonatomic, nonnull, readwrite, copy) NSString *name;
@property (nonatomic, nonnull, readwrite, copy) Closure onPerform;

@end

@implementation Action

+ (_Nonnull instancetype)actionWithName:(NSString * _Nonnull)name onPerform:(_Nonnull Closure)onPerform {
    return [[self alloc] initWithName:name onPerform:onPerform];
}

- (_Nonnull instancetype)initWithName:(NSString * _Nonnull)name onPerform:(_Nonnull Closure)onPerform {
    self = [super init];
    self.name = name;
    self.onPerform = onPerform;
    return self;
}

- (void)perform {
    self.onPerform();
}

@end
