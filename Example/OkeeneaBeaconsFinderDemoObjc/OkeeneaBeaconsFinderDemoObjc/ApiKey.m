//
//  Copyright © 2021 Okeenea. All rights reserved.
//

#import "ApiKey.h"

@implementation ApiKey

+ (NSString *)fromAppBundle {
    NSURL* url = [NSBundle.mainBundle URLForResource:@"API_KEY" withExtension:nil];
    return [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:NULL];
}

@end
