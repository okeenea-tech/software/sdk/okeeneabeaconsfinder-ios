//
//  Copyright © 2021 Okeenea. All rights reserved.
//

@import OkeeneaBeaconsFinder;
@import UIKit;

@interface BeaconDetailsViewController : UITableViewController

- (instancetype)initWithBeacon:(id<OkeeneaBeacon>)beacon;

@end
