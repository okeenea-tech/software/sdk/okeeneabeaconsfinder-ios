//
//  Copyright © 2021 Okeenea. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(nonatomic) UIWindow *window;

@end
