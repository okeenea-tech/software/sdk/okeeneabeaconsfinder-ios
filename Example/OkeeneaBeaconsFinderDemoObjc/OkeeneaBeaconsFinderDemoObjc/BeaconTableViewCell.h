//
//  Copyright © 2021 Okeenea. All rights reserved.
//

@import OkeeneaBeaconsFinder;
@import UIKit;

@interface BeaconTableViewCell : UITableViewCell

- (instancetype)updatedWith:(id<OkeeneaBeacon>)beacon;

@end
