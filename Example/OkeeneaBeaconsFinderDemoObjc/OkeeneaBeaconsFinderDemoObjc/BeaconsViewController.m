//
//  Copyright © 2021 Okeenea. All rights reserved.
//

@import OkeeneaBeaconsFinder;
@import CoreLocation;
@import CoreBluetooth;
#import "BeaconsViewController.h"
#import "ApiKey.h"
#import "BeaconTableViewCell.h"
#import "BeaconDetailsViewController.h"

static NSString *CELL_ID = @"Cell";

@implementation BeaconsViewController {
    CLLocationManager *locationManager;
    CBCentralManager *centralManager;
    BeaconsFinder *beaconsFinder;
    BOOL isStarted;
    NSArray<id <OkeeneaBeacon>> *beacons;
}

- (instancetype)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    self.title = @"Beacons Finder";
    self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Start" style:UIBarButtonItemStylePlain target:self action:@selector(toggleStart)];

    locationManager = CLLocationManager.new;
    centralManager = CBCentralManager.new;
    beaconsFinder = [[BeaconsFinder alloc] initWithApiKey:ApiKey.fromAppBundle];
    isStarted = NO;
    beacons = NSArray.new;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView registerClass:BeaconTableViewCell.class forCellReuseIdentifier:CELL_ID];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    NSIndexPath *indexPath = self.tableView.indexPathForSelectedRow;
    if (indexPath != nil) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:animated];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return beacons.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BeaconTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID forIndexPath:indexPath];
    return [cell updatedWith:[self beaconAt:indexPath]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BeaconDetailsViewController *detailsViewController = [[BeaconDetailsViewController alloc] initWithBeacon:[self beaconAt:indexPath]];
    [self.navigationController pushViewController:detailsViewController animated:YES];
}

- (void)toggleStart {
    if (isStarted) {
        [self stop];
    } else {
        [self start];
    }
}

- (void)stop {
    isStarted = NO;
    [self updateStartStopButtonTitle:@"Start"];
    [beaconsFinder stopScan];
    beacons = NSArray.new;
    [self.tableView reloadData];
}

- (void)start {
    if ([self authorized]) {
        [self startScan];
    } else {
        [self askForNecessaryAuthorizations];
    }
}

- (BOOL)authorized {
    return [self locationAuthorized] && [self bluetoothAuthorized];
}

- (BOOL)locationAuthorized {
    return CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse ||
            CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedAlways;
}

- (BOOL)bluetoothAuthorized {
    return centralManager.state == CBManagerStatePoweredOn;
}

- (void)askForNecessaryAuthorizations {
    if (![self locationAuthorized]) {
        [locationManager requestWhenInUseAuthorization];
    } else if (![self bluetoothAuthorized]) {
        [self presentAlertWithMessage:@"Bluetooth Unauthorized"];
    }
}

- (void)startScan {
    isStarted = YES;
    [self updateStartStopButtonTitle:@"Stop"];
    [beaconsFinder startScanOnUpdate:^(NSArray *foundBeacons) {
        [self didFindBeacons:foundBeacons];
    } onError:^(OkeeneaError *error) {
        [self scanDidFail:error];
    }];
}

- (void)didFindBeacons:(NSArray<id <OkeeneaBeacon>> *)foundBeacons {
    beacons = foundBeacons;
    [self.tableView reloadData];
}

- (void)scanDidFail:(OkeeneaError *)error {
    [self showError:error];
    [self stop];
}

- (void)showError:(OkeeneaError *)error {
    [self presentAlertWithMessage:error.localizedDescription];
}

- (void)presentAlertWithMessage:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)updateStartStopButtonTitle:(NSString *)title {
    self.navigationItem.rightBarButtonItem.title = title;
}

- (id <OkeeneaBeacon>)beaconAt:(NSIndexPath *)indexPath {
    return beacons[(NSUInteger) indexPath.row];
}

@end
