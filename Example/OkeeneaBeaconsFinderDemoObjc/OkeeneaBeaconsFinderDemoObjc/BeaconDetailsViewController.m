//
//  Copyright © 2021 Okeenea. All rights reserved.
//

#import "BeaconDetailsViewController.h"
#import "Action.h"

static NSString *CELL_ID = @"Cell";

@implementation BeaconDetailsViewController {
    id <OkeeneaBeacon> _beacon;
}

- (instancetype)initWithBeacon:(id <OkeeneaBeacon>)beacon {
    self = [super initWithStyle:UITableViewStyleGrouped];
    _beacon = beacon;
    self.title = _beacon.name;
    return self;
}

- (NSArray<Action *> *)actions {
    return _beacon.canBeTriggered ? [self allPossibleActions] : [self actionsWhenBeaconCannotBeTriggered];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView registerClass:UITableViewCell.class forCellReuseIdentifier:CELL_ID];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.actions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID forIndexPath:indexPath];
    cell.textLabel.text = [self actionAt:indexPath].name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[self actionAt:indexPath] perform];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (Action *)actionAt:(NSIndexPath *)indexPath {
    return self.actions[(NSUInteger) indexPath.row];
}

- (NSArray<Action *> *)allPossibleActions {
    typeof(self) __weak weakSelf = self;
    id <OkeeneaBeacon> __weak _beacon = self->_beacon;
    return @[
            [Action actionWithName:@"Trigger 'Jingle'" onPerform:^{
                [_beacon triggerWithMessage:MessageKindJingle volume:VolumeMedium language:LanguagePreferenceEnglish completionHandler:^(NSError *error) {
                    [weakSelf didComplete:@"Trigger 'Jingle'" error:error];
                }];
            }],
            [Action actionWithName:@"Trigger 'Message1'" onPerform:^{
                [_beacon triggerWithMessage:MessageKindMessage1 volume:VolumeMedium language:LanguagePreferenceEnglish completionHandler:^(NSError *error) {
                    [weakSelf didComplete:@"Trigger 'Message1'" error:error];
                }];
            }],
            [Action actionWithName:@"Trigger 'Message2'" onPerform:^{
                [_beacon triggerWithMessage:MessageKindMessage2 volume:VolumeMedium language:LanguagePreferenceEnglish completionHandler:^(NSError *error) {
                    [weakSelf didComplete:@"Trigger 'Message2'" error:error];
                }];
            }],
            [Action actionWithName:@"Trigger 'Message3'" onPerform:^{
                [_beacon triggerWithMessage:MessageKindMessage3 volume:VolumeMedium language:LanguagePreferenceEnglish completionHandler:^(NSError *error) {
                    [weakSelf didComplete:@"Trigger 'Message3'" error:error];
                }];
            }],
            [Action actionWithName:@"Repeat last message" onPerform:^{
                [_beacon repeatLastMessageWithCompletionHandler:^(NSError *error) {
                    [weakSelf didComplete:@"Repeat last message'" error:error];
                }];
            }],
            [Action actionWithName:@"Play next message" onPerform:^{
                [_beacon playNextMessageWithCompletionHandler:^(NSError *error) {
                    [weakSelf didComplete:@"Play next message'" error:error];
                }];
            }],
            [Action actionWithName:@"Cancel" onPerform:^{
                [_beacon cancelPlayedMessageWithCompletionHandler:^(NSError *error) {
                    [weakSelf didComplete:@"Cancel played message" error:error];
                }];
            }]
    ];
}

- (void)didComplete:(NSString *)actionDescription error:(NSError *)error {
    if (error != nil) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:error.localizedDescription message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        NSLog(@"Did complete '%@'.", actionDescription);
    }
}

- (NSArray<Action *> *)actionsWhenBeaconCannotBeTriggered {
    return @[[Action actionWithName:@"This beacon cannot be triggered" onPerform:^{
    }]];
}

@end
