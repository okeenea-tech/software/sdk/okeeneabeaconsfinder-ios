//
//  Copyright © 2021 Okeenea. All rights reserved.
//

@import Foundation;

typedef void (^Closure)(void);

@interface Action : NSObject
@property (nonatomic, nonnull, readonly, copy) NSString *name;

+ (_Nonnull instancetype)actionWithName:(NSString * _Nonnull)name onPerform:(_Nonnull Closure)onPerform;
- (_Nonnull instancetype)initWithName:(NSString * _Nonnull)name onPerform:(_Nonnull Closure)onPerform;

- (void)perform;

@end
