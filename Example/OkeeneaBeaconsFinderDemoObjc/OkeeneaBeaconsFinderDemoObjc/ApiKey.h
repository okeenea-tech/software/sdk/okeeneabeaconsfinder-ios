//
//  Copyright © 2021 Okeenea. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiKey : NSObject

+ (NSString *)fromAppBundle;

@end
