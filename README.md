# Okeenea BeaconsFinder SDK

## Installation

### CocoaPods

```ruby
pod 'OkeeneaBeaconsFinder'
```

## Usage

**Pre-requisite** :

- Ask for Location authorization (to range CLBeacons).
- Ask for Bluetooth authorization (to scan for CBPeripherals).

The SDK will not ask for these authorizations, it will just check them and report errors.

### Note about completion handlers

The completion handler informs when communication with the beacon has been done. **It does not tell that the beacon did finish playing a message.**

You can use it to report any communication error to the user or to know that the action (ex: trigger) did succeed.

### Demo app

You can try the SDK using the demo app in `Example/OkeeneaBeaconsFinderDemoSwift` (Swift) or `Example/OkeeneaBeaconsFinderDemoObjc` (Objective-C).

You will have to put your API key in the file `Example/OkeeneaBeaconsFinderDemoSwift/API_KEY` (Swift) or `Example/OkeeneaBeaconsFinderDemoObjc/OkeeneaBeaconsFinderDemoObjc/API_KEY` (Objective-C) then run the app on a physical device (the simulator is not supported because of bluetooth needs).

### Swift

```swift
// Initialize the SDK
let beaconsFinder = BeaconsFinder(apiKey: "abc", languageForBeaconNames: LanguagePreference.devicePreferredLanguage) // for other languages see LanguagePreference enum

// Start scan
beaconsFinder.startScan(onUpdate: { (beacons: [OkeeneaBeacon]) in
    // usually called once per second with updated found beacons (name updates, range updates, etc.)
}, onError: { (error: OkeeneaError) in
    // handle the error
})

// Stop scan
beaconsFinder.stopScan()
```

```swift
@objc
public enum LanguagePreference: Int, CaseIterable, CustomDebugStringConvertible {
    
    case french
    case english
    case spanish
    case german
    case dutch
    case portuguese
    case italian
    case chinese
    case japanese
    case korean
    case vietnamese
    case russian
    case swedish
    case danish
    case finnish
    case norwegian
    case arabic
    case turkish
    case greek
    case indonesian
    case malay
    case thai
    case hindi
    case hungarian
    case polish
    case czech
    case slovak
    case ukrainian
    case catalan
    case romanian
    case croatian
    case hebrew
    
    public var isoLanguageCode: String
    
    public var localizedName: String
    
    public static var devicePreferredLanguage: Self
    
    public static func fromStringOrDefault(_ rawValue: String?) -> Self
}
```

```swift
public class OkeeneaError: NSError {
}

@objc
public enum OkeeneaErrorCodes: Int {
    case invalidAPIKey
    case apiKeyVerificationFailed
    case locationUnauthorized
    case trackingError
    case other
}
```

```swift
@objc
protocol OkeeneaBeacon {
    
    /// Proximity's information
    var proximity: BeaconProximity { get }
    
    /// Beacon's kind
    var kind: BeaconKind { get }
    
    /// Name of the beacon
    var name: String { get }
    
    /// Know if the beacon can be triggered
    var canBeTriggered: Bool { get }
    
    /// Trigger the beacon
    func trigger(message: MessageKind, volume: Volume, language: LanguagePreference, completionHandler: @escaping OkeeneaBeaconCompletionHandler)
    
    /// Repeat the last message
    func repeatLastMessage(completionHandler: @escaping OkeeneaBeaconCompletionHandler)
    
    /// Play the next message
    func playNextMessage(completionHandler: @escaping OkeeneaBeaconCompletionHandler)
    
    /// Cancel the message currently played
    func cancelPlayedMessage(completionHandler: @escaping OkeeneaBeaconCompletionHandler)
}

typealias OkeeneaBeaconCompletionHandler = (Error?) -> Void

public class BeaconProximity: NSObject {
    
    @objc
    public init(timestamp: Date? = nil, rssi: Int = 0, proximity: CLProximity = .unknown, accuracy: CLLocationAccuracy = -1)
    
    @objc
    public let timestamp: Date?
    @objc
    public let rssi: Int
    @objc
    public let proximity: CLProximity
    @objc
    public let accuracy: CLLocationAccuracy
}

@objc
public enum BeaconKind: Int {
    case pointOfInterest
    case audiblePedestrianSignal
}

@objc
public enum MessageKind: Int {
    case jingle
    case jingleAndMessage1
    case message1
    case message2
    case message3
    case all
}

@objc
public enum Volume: Int {
    case low
    case medium
    case high
}
```
