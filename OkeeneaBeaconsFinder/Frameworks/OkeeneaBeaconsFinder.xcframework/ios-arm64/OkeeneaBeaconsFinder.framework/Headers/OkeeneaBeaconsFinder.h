//
//  OkeeneaBeaconsFinder.h
//  OkeeneaBeaconsFinder
//
//  Created by Nicolas VERINAUD on 18/11/2021.
//  Copyright © 2021 Okeenea. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for OkeeneaBeaconsFinder.
FOUNDATION_EXPORT double OkeeneaBeaconsFinderVersionNumber;

//! Project version string for OkeeneaBeaconsFinder.
FOUNDATION_EXPORT const unsigned char OkeeneaBeaconsFinderVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OkeeneaBeaconsFinder/PublicHeader.h>


