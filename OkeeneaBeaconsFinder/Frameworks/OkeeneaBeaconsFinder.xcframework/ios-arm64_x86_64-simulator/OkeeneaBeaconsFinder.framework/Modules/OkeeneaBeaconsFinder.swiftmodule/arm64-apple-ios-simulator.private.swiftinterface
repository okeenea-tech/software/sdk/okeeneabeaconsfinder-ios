// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 6.0.3 effective-5.10 (swiftlang-6.0.3.1.10 clang-1600.0.30.1)
// swift-module-flags: -target arm64-apple-ios12.4-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name OkeeneaBeaconsFinder
// swift-module-flags-ignorable: -no-verify-emitted-module-interface
import AVFoundation
import CoreBluetooth
import CoreLocation
import Foundation
@_exported import OkeeneaBeaconsFinder
import RswiftResources
import Swift
import SwifterSwift
import UIKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
@objc @_hasMissingDesignatedInitializers public class BeaconsFinder : ObjectiveC.NSObject {
  @objc convenience public init(apiKey: Swift.String)
  @objc convenience public init(apiKey: Swift.String, languageForBeaconNames language: OkeeneaBeaconsFinder.LanguagePreference)
  @objc public func startScan(onUpdate: @escaping ([any OkeeneaBeaconsFinder.OkeeneaBeacon]) -> Swift.Void, onError: @escaping (OkeeneaBeaconsFinder.OkeeneaError) -> Swift.Void)
  @objc public func stopScan()
  @objc deinit
}
@objc public enum OkeeneaErrorCodes : Swift.Int {
  case invalidAPIKey = 1
  case apiKeyVerificationFailed = 2
  case locationUnauthorized = 3
  case trackingError = 4
  case other = 5
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers public class OkeeneaError : Foundation.NSError, @unchecked Swift.Sendable {
  @objc override dynamic public var hash: Swift.Int {
    @objc get
  }
  @objc override dynamic public func isEqual(_ object: Any?) -> Swift.Bool
  @objc override dynamic public init(domain: Swift.String, code: Swift.Int, userInfo dict: [Swift.String : Any]? = nil)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@objc public protocol OkeeneaBeacon {
  @objc var proximity: OkeeneaBeaconsFinder.BeaconProximity { get }
  @objc var kind: OkeeneaBeaconsFinder.BeaconKind { get }
  @objc var name: Swift.String { get }
  @objc var canBeTriggered: Swift.Bool { get }
  @objc func trigger(message: OkeeneaBeaconsFinder.MessageKind, volume: OkeeneaBeaconsFinder.Volume, language: OkeeneaBeaconsFinder.LanguagePreference, completionHandler: @escaping OkeeneaBeaconsFinder.OkeeneaBeaconCompletionHandler)
  @objc func repeatLastMessage(completionHandler: @escaping OkeeneaBeaconsFinder.OkeeneaBeaconCompletionHandler)
  @objc func playNextMessage(completionHandler: @escaping OkeeneaBeaconsFinder.OkeeneaBeaconCompletionHandler)
  @objc func cancelPlayedMessage(completionHandler: @escaping OkeeneaBeaconsFinder.OkeeneaBeaconCompletionHandler)
}
public typealias OkeeneaBeaconCompletionHandler = ((any Swift.Error)?) -> Swift.Void
@objc public class BeaconProximity : ObjectiveC.NSObject {
  @objc public init(timestamp: Foundation.Date? = nil, rssi: Swift.Int = 0, proximity: CoreLocation.CLProximity = .unknown, accuracy: CoreLocation.CLLocationAccuracy = -1)
  @objc final public let timestamp: Foundation.Date?
  @objc final public let rssi: Swift.Int
  @objc final public let proximity: CoreLocation.CLProximity
  @objc final public let accuracy: CoreLocation.CLLocationAccuracy
  @objc override dynamic public var hash: Swift.Int {
    @objc get
  }
  @objc override dynamic public func isEqual(_ object: Any?) -> Swift.Bool
  @objc deinit
}
@objc public enum BeaconKind : Swift.Int {
  case pointOfInterest
  case audiblePedestrianSignal
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public enum MessageKind : Swift.Int {
  case jingle
  case jingleAndMessage1
  case message1
  case message2
  case message3
  case all
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public enum Volume : Swift.Int {
  case low
  case medium
  case high
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum BeaconTrackingError : Swift.Error, Swift.Equatable {
  case settingsIssue(reason: Swift.String, recoverySuggestion: Swift.String)
  case other(Swift.String)
  public var reason: Swift.String {
    get
  }
  public var recoverySuggestion: Swift.String {
    get
  }
  public static func == (a: OkeeneaBeaconsFinder.BeaconTrackingError, b: OkeeneaBeaconsFinder.BeaconTrackingError) -> Swift.Bool
}
#if compiler(>=5.3) && $RetroactiveAttribute
extension Swift.String : @retroactive Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
#else
extension Swift.String : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
#endif
@objc public enum LanguagePreference : Swift.Int, Swift.CaseIterable, Swift.CustomDebugStringConvertible {
  case french
  case english
  case spanish
  case german
  case dutch
  case portuguese
  case italian
  case chinese
  case japanese
  case korean
  case vietnamese
  case russian
  case swedish
  case danish
  case finnish
  case norwegian
  case arabic
  case turkish
  case greek
  case indonesian
  case malay
  case thai
  case hindi
  case hungarian
  case polish
  case czech
  case slovak
  case ukrainian
  case catalan
  case romanian
  case croatian
  case hebrew
  public var isoLanguageCode: Swift.String {
    get
  }
  public var localizedName: Swift.String {
    get
  }
  public static var devicePreferredLanguage: OkeeneaBeaconsFinder.LanguagePreference {
    get
  }
  public static func fromStringOrDefault(_ rawValue: Swift.String?) -> OkeeneaBeaconsFinder.LanguagePreference
  public var debugDescription: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias AllCases = [OkeeneaBeaconsFinder.LanguagePreference]
  public typealias RawValue = Swift.Int
  nonisolated public static var allCases: [OkeeneaBeaconsFinder.LanguagePreference] {
    get
  }
  public var rawValue: Swift.Int {
    get
  }
}
public protocol AnalyticsTracker {
  func track(event: OkeeneaBeaconsFinder.AnalyticsEvent)
}
public struct AnalyticsEvent : Swift.Equatable {
  public var name: OkeeneaBeaconsFinder.AnalyticsEvent.Name
  public var properties: [Swift.String : Any]
  public static func == (lhs: OkeeneaBeaconsFinder.AnalyticsEvent, rhs: OkeeneaBeaconsFinder.AnalyticsEvent) -> Swift.Bool
  public static func stopBusGiveFeedback() -> OkeeneaBeaconsFinder.AnalyticsEvent
  public enum Name : Swift.String {
    case stopBusGiveFeedback
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
}
extension OkeeneaBeaconsFinder.OkeeneaErrorCodes : Swift.Equatable {}
extension OkeeneaBeaconsFinder.OkeeneaErrorCodes : Swift.Hashable {}
extension OkeeneaBeaconsFinder.OkeeneaErrorCodes : Swift.RawRepresentable {}
extension OkeeneaBeaconsFinder.BeaconKind : Swift.Equatable {}
extension OkeeneaBeaconsFinder.BeaconKind : Swift.Hashable {}
extension OkeeneaBeaconsFinder.BeaconKind : Swift.RawRepresentable {}
extension OkeeneaBeaconsFinder.MessageKind : Swift.Equatable {}
extension OkeeneaBeaconsFinder.MessageKind : Swift.Hashable {}
extension OkeeneaBeaconsFinder.MessageKind : Swift.RawRepresentable {}
extension OkeeneaBeaconsFinder.Volume : Swift.Equatable {}
extension OkeeneaBeaconsFinder.Volume : Swift.Hashable {}
extension OkeeneaBeaconsFinder.Volume : Swift.RawRepresentable {}
extension OkeeneaBeaconsFinder.LanguagePreference : Swift.Equatable {}
extension OkeeneaBeaconsFinder.LanguagePreference : Swift.Hashable {}
extension OkeeneaBeaconsFinder.LanguagePreference : Swift.RawRepresentable {}
extension OkeeneaBeaconsFinder.AnalyticsEvent.Name : Swift.Equatable {}
extension OkeeneaBeaconsFinder.AnalyticsEvent.Name : Swift.Hashable {}
extension OkeeneaBeaconsFinder.AnalyticsEvent.Name : Swift.RawRepresentable {}
